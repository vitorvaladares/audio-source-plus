﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

///Versão 0.91 - Última atualização 29/08/2016 - 15:40
///Audio Source Plus
///     Preview dos efeitos
///     Tocar multiplos clips em diversas ordens
///     Randomizar o volume e o pitch
///     Adaptado para funcionar com Unity 4.x e 5.x
///     
///Melhorias
///Tocar sem efeitos de output
///Expor e ranzomizar delay

public enum AudioSourceType
{
    singleClip,
    multiplesClips,
}

public enum PlayMode
{
    randomizeOne, 
    avoidRepeat,
    playAllBeforeRepeat,
    playInSequence,
}

[System.Serializable]
[ExecuteInEditMode]
[RequireComponent(typeof(AudioSource))]
[AddComponentMenu("Audio/Audio Source Plus", 100)]
public class AudioSourcePlus : MonoBehaviour
{
    public new AudioSource audio
    {
        get
        {
            if (_audioSource == null)
            {
                _audioSource = GetComponent<AudioSource>();
            }
            return _audioSource;
        }
    }

    public AudioSource _audioSource;

    public AudioClip[] clips;

    public AudioSourceType sourceType;

    public PlayMode playMode;

    public bool useRandomVolume, useRandomPitch, playOnAwake;

    public static bool Show3DSettings, ShowPreview;

    [Range(0, 1)]
    public float minVol, maxVol = 1;
    [Range(-3, 3)]
    public float minPitch = .95F, maxPitch = 1.05F;
    
    private float defaultVolume, defaultPitch;

    private bool _dontStopPlaying, _firstLoopDone;

    private int _currentClipToPlay;

    private List<AudioClip> _tempList;

    public void Awake()
    {
        if (Application.isPlaying)
        {
            audio.clip = null;

            if (playOnAwake)
            {
                Play();
            }
        }
    }
    public void Play()
    {
        switch (sourceType)
        {
            case AudioSourceType.singleClip:
                UpdateSettings();
                audio.clip = clips[0];
                audio.Play();
                break;
            case AudioSourceType.multiplesClips:
                _currentClipToPlay = 0;
                _tempList = null;
                _firstLoopDone = false;

                if (audio.loop || playMode == PlayMode.playAllBeforeRepeat || playMode == PlayMode.playInSequence)
                {
                    StartCoroutine(PlayMultiples());
                }
                else
                {
                    UpdateSettings();

                    SetClip();

                    audio.Play();
                }
                break;
            default:
                break;
        }
    }
    public void PlayOneShot(AudioClip clip)
    {
        UpdateSettings();

        audio.PlayOneShot(clip);
    }
    private IEnumerator PlayMultiples()
    {
        _dontStopPlaying = true;

        audio.clip = null;

        while (_dontStopPlaying && this.gameObject)
        {
            UpdateSettings();

            SetClip();

            audio.PlayOneShot(audio.clip);

            yield return new WaitForSeconds(audio.clip.length);

            if (!audio.loop && _firstLoopDone)
                _dontStopPlaying = false;
        }
    }
    public void PlayOneShot()
    {
        UpdateSettings();

        switch (sourceType)
        {
            case AudioSourceType.singleClip:
                audio.clip = clips[0];
                audio.PlayOneShot(clips[0]);
                break;
            case AudioSourceType.multiplesClips:
                SetClip();
                break;
            default:
                break;
        }

        audio.PlayOneShot(audio.clip);
    }
    private void SetClip()
    {
        switch (playMode)
        {
            case PlayMode.randomizeOne:
                audio.clip = clips[Random.Range(0, clips.Length)];
                break;
            case PlayMode.playInSequence:
                if (_currentClipToPlay >= clips.Length)
                {
                    _currentClipToPlay = 0;
                }

                audio.clip = clips[_currentClipToPlay];

                _currentClipToPlay++;

                if (_currentClipToPlay >= clips.Length)
                {
                    _firstLoopDone = true;
                }
                break;
            case PlayMode.avoidRepeat:
                {
                    var list = clips.ToList();

                    if (audio.clip != null)
                        list.Remove(audio.clip);

                    audio.clip = list[Random.Range(0, list.Count)];
                }
                break;
            case PlayMode.playAllBeforeRepeat:
                {
                    if (_tempList == null)
                        _tempList = new List<AudioClip>();

                    if (_tempList.Count < 1)
                        _tempList.AddRange(clips);

                    audio.clip = _tempList[Random.Range(0, _tempList.Count)];

                    if (audio.clip != null)
                        _tempList.Remove(audio.clip);

                    _firstLoopDone = _tempList.Count < 1;
                }
                break;
            default:
                break;
        }
    }
    public void Stop()
    {
        _dontStopPlaying = false;

        audio.Stop();
    }
    private void UpdateSettings()
    {
        defaultVolume = audio.volume;
        defaultPitch = audio.pitch;

        audio.volume = useRandomVolume ? Random.Range(minVol, maxVol) : defaultVolume;
        audio.pitch = useRandomPitch ? Random.Range(minPitch, maxPitch) : defaultPitch;
    }
    public void PlayFadeIn(float duration)
    {
        UpdateSettings();

        switch (sourceType)
        {
            case AudioSourceType.singleClip:
                audio.clip = clips[0];
                break;
            case AudioSourceType.multiplesClips:
                SetClip();
                break;
            default:
                break;
        }

        StartCoroutine(FadeInVolume(duration));

        audio.Play();
    }
    public void StopFadeOut(float duration)
    {
        StartCoroutine(FadeOutVolume(duration));
    }
    private IEnumerator FadeInVolume(float duration)
    {
        var timer = 0F;

        while (timer < duration)
        {
            timer += Time.deltaTime;

            audio.volume = Mathf.Lerp(0, defaultVolume, timer / duration);

            yield return null;
        }

        audio.volume = defaultVolume;
    }
    private IEnumerator FadeOutVolume(float duration)
    {
        var timer = 0F;
        var currentVol = audio.volume;

        while (timer < duration)
        {
            timer += Time.deltaTime;

            audio.volume = Mathf.Lerp(currentVol, 0, timer / duration);

            yield return null;
        }

        Stop();

        audio.volume = defaultVolume;
    }
}
