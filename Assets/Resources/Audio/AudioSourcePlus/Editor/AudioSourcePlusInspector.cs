﻿using UnityEngine;
using UnityEditor;
#if UNITY_5
using UnityEngine.Audio;
#endif

[CustomEditor(typeof(AudioSourcePlus))]
public class AudioSourceExtraInspector : Editor
{
    public GUISkin hintSkin, buttonSkin;

    private AudioSourcePlus _target;

    private Texture _playingIconOn, _playingIconOff, _playing1sIconOn, _playing1sIconOff;

    private void OnEnable()
    {
        _target = (AudioSourcePlus)target;
    }
    private void LoadAssets()
    {
        if (hintSkin == null)
        {

            hintSkin = CreateInstance<GUISkin>();

            var c = 100 / 255F;
            hintSkin.label.normal.textColor = new Color(c, c, c);

            hintSkin.label.margin = new RectOffset(0, 0, 0, 0);
            hintSkin.label.padding = new RectOffset(0, 0, -5, 0);

            hintSkin.label.fontSize = 9;
        }

        if (buttonSkin == null)
        {
            buttonSkin = Resources.Load("Audio/AudioSourcePlus/buttonGuiSkin", typeof(GUISkin)) as GUISkin;
        }

        if (_playingIconOn == null)
        {
            _playingIconOn = Resources.Load("Audio/AudioSourcePlus/Icon/AudioSourcePlus_TEX_Button_PlayOn", typeof(Texture)) as Texture;
        }

        if (_playingIconOff == null)
        {
            _playingIconOff = Resources.Load("Audio/AudioSourcePlus/Icon/AudioSourcePlus_TEX_Button_PlayOff", typeof(Texture)) as Texture;
        }

        if (_playing1sIconOn == null)
        {
            _playing1sIconOn = Resources.Load("Audio/AudioSourcePlus/Icon/AudioSourcePlus_TEX_Button_PlayOneShotOn", typeof(Texture)) as Texture;
        }

        if (_playing1sIconOff == null)
        {
            _playing1sIconOff = Resources.Load("Audio/AudioSourcePlus/Icon/AudioSourcePlus_TEX_Button_PlayOneShotOff", typeof(Texture)) as Texture;
        }
    }
    public override void OnInspectorGUI()
    {
        LoadAssets();

        if (_target.clips == null)
            _target.clips = new AudioClip[1];

        GUILayout.Space(10);

        _target.sourceType = (AudioSourceType)EditorGUILayout.EnumPopup("Source Type", _target.sourceType);

        switch (_target.sourceType)
        {
            case AudioSourceType.singleClip:
                _target.audio.clip = _target.clips[0] = (AudioClip)EditorGUILayout.ObjectField("Single Clip", _target.clips[0], typeof(AudioClip), false);
                break;
            case AudioSourceType.multiplesClips:
                _target.playMode = (PlayMode)EditorGUILayout.EnumPopup("Play Mode", _target.playMode);

                var property = serializedObject.FindProperty("clips");
                EditorGUILayout.PropertyField(property, new GUIContent("Multiple Clips"), true);
                serializedObject.ApplyModifiedProperties();
                break;
            default:
                break;
        }

        GUILayout.Space(10);
		#if UNITY_5
        _target.audio.outputAudioMixerGroup = (AudioMixerGroup)EditorGUILayout.ObjectField("Output", _target.audio.outputAudioMixerGroup, typeof(AudioMixerGroup), false);
        #endif
        GUILayout.Space(10);

        AudioSourcePlus.ShowPreview = EditorGUILayout.Foldout(AudioSourcePlus.ShowPreview, "Preview");

        if (AudioSourcePlus.ShowPreview)
        {
            GUILayout.BeginVertical("box");
            {
                GUILayout.Space(5);

                GUILayout.BeginHorizontal();
                {
                    GUI.skin = buttonSkin;

                    GUILayout.FlexibleSpace();

                    GUI.enabled = Application.isPlaying;

                    if (GUILayout.Button(_target.audio.isPlaying ? _playingIconOn : _playingIconOff, GUILayout.Height(32)))
                    {
                        if (_target.audio.isPlaying)
                        {
                            _target.Stop();
                        }
                        else
                        {
                            _target.Play();
                        }
                    }

                    GUI.enabled = true;

                    GUILayout.FlexibleSpace();

                    if (GUILayout.Button(_target.audio.isPlaying ? _playing1sIconOn : _playing1sIconOff, GUILayout.Height(32)))
                    {
                        if (_target.audio.isPlaying)
                        {
                            _target.Stop();
                        }

                        _target.PlayOneShot();
                    }

                    GUILayout.FlexibleSpace();

                    GUI.skin = null;
                }
                GUILayout.EndHorizontal();
                GUILayout.Space(5);
            }
            GUILayout.EndVertical();
        }

        GUILayout.Space(10);


        _target.audio.mute = EditorGUILayout.Toggle("Mute", _target.audio.mute);
        _target.audio.bypassEffects = EditorGUILayout.Toggle("Bypass Effects", _target.audio.bypassEffects);
        _target.audio.bypassListenerEffects = EditorGUILayout.Toggle("Bypass Listener Effects", _target.audio.bypassListenerEffects);
        _target.audio.bypassReverbZones = EditorGUILayout.Toggle("Bypass Reverb Zones", _target.audio.bypassReverbZones);

        _target.audio.playOnAwake = false;
        GUIContent content = new GUIContent("Play On Awake", "Play the sound when the scene loads.");
        _target.playOnAwake = EditorGUILayout.Toggle(content, _target.playOnAwake);

        _target.audio.loop = EditorGUILayout.Toggle("Loop", _target.audio.loop);

        GUILayout.Space(10);

        _target.audio.priority = EditorGUILayout.IntSlider("Priority", _target.audio.priority, 0, 256);

        DrawBorderHints("High", "Low");

        _target.audio.volume = EditorGUILayout.Slider("Volume", _target.audio.volume, 0, 1);

        GUILayout.BeginVertical("box");

        _target.useRandomVolume = EditorGUILayout.Toggle("Use Random Volume", _target.useRandomVolume);

        if (_target.useRandomVolume)
        {
            _target.minVol = EditorGUILayout.Slider("Min Volume", _target.minVol, 0, _target.maxVol);
            _target.maxVol = EditorGUILayout.Slider("Max Volume", _target.maxVol, _target.minVol, 1);
        }

        GUILayout.EndVertical();

        GUILayout.Space(10);

        _target.audio.pitch = EditorGUILayout.Slider("Pitch", _target.audio.pitch, -3, 3);
        DrawBorderHints("-3.0", "3.0");

        GUILayout.BeginVertical("box");

        _target.useRandomPitch = EditorGUILayout.Toggle("Use Random Pitch", _target.useRandomPitch);

        if (_target.useRandomPitch)
        {
            _target.minPitch = EditorGUILayout.Slider("Min Pitch", _target.minPitch, -3, _target.maxPitch);
            _target.maxPitch = EditorGUILayout.Slider("Max Pitch", _target.maxPitch, _target.minPitch, 3);
        }

        GUILayout.EndVertical();

        GUILayout.Space(10);

		#if UNITY_5
        _target.audio.panStereo = EditorGUILayout.Slider("Stereo Pan", _target.audio.panStereo, -1, 1);

        DrawBorderHints("Left", "Right");

        _target.audio.spatialBlend = EditorGUILayout.Slider("Spatial Blend", _target.audio.spatialBlend, 0, 1);

        DrawBorderHints("2D", "3D");

        _target.audio.reverbZoneMix = EditorGUILayout.Slider("Reverb Zone Mix", _target.audio.reverbZoneMix, 0, 1.1F);

        GUILayout.Space(10);
#endif

        AudioSourcePlus.Show3DSettings = EditorGUILayout.Foldout(AudioSourcePlus.Show3DSettings, "3D Sound Settings");

        if (AudioSourcePlus.Show3DSettings)
        {
            _target.audio.dopplerLevel = EditorGUILayout.Slider("Doppler Level", _target.audio.dopplerLevel, 0, 5);
            _target.audio.spread = EditorGUILayout.Slider("Spread", _target.audio.spread, 1, 360);

            _target.audio.rolloffMode = (AudioRolloffMode) EditorGUILayout.EnumPopup("Volume Rollof", _target.audio.rolloffMode);

            _target.audio.minDistance = EditorGUILayout.FloatField("Min Distance", _target.audio.minDistance);
            _target.audio.maxDistance = EditorGUILayout.FloatField("Max Distance", _target.audio.maxDistance);

            EditorGUILayout.HelpBox("Please use the default 'Audio Source' component in order to edit the 3D Curves Settings", MessageType.Info);
        }
    }
    private void DrawBorderHints(string leftHint, string rightHint)
    {
        GUILayout.BeginHorizontal();

        GUI.skin = hintSkin;

        GUI.skin.label.alignment = TextAnchor.UpperLeft;

        EditorGUILayout.PrefixLabel(" ");

        GUILayout.Label(leftHint);

        GUI.skin.label.alignment = TextAnchor.UpperRight;

        GUILayout.Label(rightHint);

        GUILayout.Space(55);

        GUI.skin = null;

        GUILayout.EndHorizontal();

        GUILayout.Space(-5);
    }
}
